Source: xnec2c
Section: hamradio
Priority: optional
Maintainer: Debian Hamradio Maintainers <debian-hams@lists.debian.org>
Uploaders:
 Dave Hibberd <hibby@debian.org>,
Build-Depends: debhelper-compat (= 13),libgtk-3-dev,intltool
Standards-Version: 4.7.0.1
Vcs-Browser: https://salsa.debian.org/debian-hamradio-team/xnec2c
Vcs-Git: https://salsa.debian.org/debian-hamradio-team/xnec2c.git
Homepage: https://github.com/KJ7LNW/xnec2c
Rules-Requires-Root: no

Package: xnec2c
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 librsvg2-common,
Description: calculate and display radio antenna properties
 The original nec2c is a non-interactive command-line application that reads
 standard NEC2 input files and produces an output file with data requested
 by "commands" in the input file.
 .
 In contrast xnec2c is a GUI interactive application that (in its current form)
 reads NEC2 input files but presents output data in graphical form, e.g. as
 wire frame drawings of the radiation pattern or near E/H field, graphs of
 maximum gain, input impedance, vswr etc against frequency and simple rendering
 of the antenna structure, including color code representation of currents or
 charge densities. These results are only calculated and drawn on user demand
 via menu items or buttons, e.g. xnec2c is interactive and does not execute
 NEC2 "commands" in batch style as the original does.
